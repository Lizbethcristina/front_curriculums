import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrocurriculumComponent } from './registrocurriculum.component';

describe('RegistrocurriculumComponent', () => {
  let component: RegistrocurriculumComponent;
  let fixture: ComponentFixture<RegistrocurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrocurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrocurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
