import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrocurriculumComponent } from './registrocurriculum.component';

const routes: Routes = [
  {
    path: '',
    component: RegistrocurriculumComponent,
    data: {
      title: 'Registrocurriculum'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrocurriculumRoutingModule {}
