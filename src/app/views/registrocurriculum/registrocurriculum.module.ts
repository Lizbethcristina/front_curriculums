import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
//import { ModalComponent } from '../../directivas';
//import { ModalService } from '../../services/modal.service';

import { FormularioService } from '../../services/formulario.service';

import { RegistrocurriculumComponent } from './registrocurriculum.component';
import { RegistrocurriculumRoutingModule } from './registrocurriculum-routing.module';


import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';


import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    RegistrocurriculumRoutingModule,
    FormsModule ,
    CommonModule,
    ModalModule.forRoot()
  ],

  declarations: [ RegistrocurriculumComponent],
  providers : [FormularioService]
})
export class RegistrocurriculumModule { }
