import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Inicio',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Registro',
    url: '',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Registro de curriculum',
        url: '/registrocurriculum',
        icon: 'icon-puzzle'
      }
    ]
  },
  
];
