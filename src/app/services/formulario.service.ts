import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Formulario } from '../models/formulario';
import { GLOBAL } from './global';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';




@Injectable()
export class FormularioService {

    public url: string;

                            constructor(public _http: HttpClient) {this.url = GLOBAL.url;

                            }

                            getFormularioes(page = null): Observable<any> {
                                const headers = new HttpHeaders().set('Content-Type', 'application/json');
                                
                            return this._http.get(this.url + 'formularioes/' + page , { headers: headers});
                            }

                           
}
