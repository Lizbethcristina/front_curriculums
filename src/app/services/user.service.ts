import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import {  Usuario } from '../models/usuario';


@Injectable()
export class UserService {
    token: string;
    identity: any;

    public url: string;
    public control: string;

    constructor(public _http: HttpClient) {
 this.url = GLOBAL.url;
 this.control = GLOBAL.controlacceso;
    }


login( user: Usuario , gettoken= null): Observable<any> {
    if (gettoken != null) {
        user.gettoken = gettoken;
    }
    const params = JSON.stringify(user);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
       return this._http.post(this.url + 'login', params , {headers: headers}) ;

}

getIdentify() {
    const identity = JSON.parse(localStorage.getItem('identity'));
    if ( identity !== 'undefined' ) {
        this.identity = identity;
    } else {
        this.identity = null;
    }
    return this.identity ;
}


getToken() {
    const token = localStorage.getItem('token');
    if ( token !== 'undefined' ) {
        this.token = token;
    } else {
        this.token = null;
    }
    return this.token;
}







    register() {
        console.log(this.url);
    }





    registrarlogin(user: Usuario): Observable<any> {
        const params = JSON.stringify({});
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('password', user.contrasena);
        headers = headers.append('usuario', user.usuario);
        headers = headers.append('ip', user.ip);
        headers = headers.append('aplicacion', user.aplicacion);
           return this._http.post(this.control , params, {headers: headers}) ;
        }
}
