export class Formulario {
    constructor (
       public nombre: string,
       public apellido: string,
       public direccion: string,
       public telefono: string,
       public descripcion: string,
       public porcentaje: Number,
       public url: string,
       public imagen: string,
       public codigo: Number,
       public descripcion_curso: string,
       public nombre_curso: Number,
       public dep: string,
       public nivel: Number,
       public createdAt: String,
       public cod: Number,

                   ) {}


   }
